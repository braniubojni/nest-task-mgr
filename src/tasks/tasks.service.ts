import { Injectable, NotFoundException } from '@nestjs/common';
import { ITask, TaskStatus } from './task.model';
import * as uuid from 'uuid';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';

@Injectable()
export class TasksService {
  private tasks: ITask[] = [
    {
      id: '1',
      title: 'casual',
      description: 'Do the loundry',
      status: TaskStatus.IN_PROGRESS,
    },
    {
      id: '2',
      title: 'casual',
      description: 'Clean the house',
      status: TaskStatus.OPEN,
    },
  ];

  getTasks(filterDto: GetTasksFilterDto): ITask[] {
    const { status, search } = filterDto;
    let tasks = this.tasks;

    if (status) {
      tasks = tasks.filter((task) => task.status === status);
    }
    if (search) {
      const filter = search.toLowerCase();
      tasks = tasks.filter(
        ({ title, description }: ITask) =>
          title.toLowerCase().includes(filter) ||
          description.toLowerCase().includes(filter),
      );
    }

    return tasks;
  }
  getByOne(id: string): ITask {
    const founded = this.tasks.find((task) => task.id === id);
    if (!founded) {
      throw new NotFoundException(`Task with id ${id} was not found`);
    }

    return founded;
  }

  createTask(createTaskDto: CreateTaskDto): ITask {
    const { title, description } = createTaskDto;
    const task = {
      id: uuid.v4(),
      title: title,
      description,
      status: TaskStatus.OPEN,
    };
    this.tasks.push(task);

    return task;
  }

  deleteTask(id: string): ITask {
    const deletedTask = this.getByOne(id);

    this.tasks = this.tasks.filter((task) => task.id !== deletedTask.id);

    return deletedTask;
  }

  updateTaskStatus(
    id: string,
    updateTaskStatusDto: UpdateTaskStatusDto,
  ): ITask {
    const task = this.getByOne(id);
    const { status } = updateTaskStatusDto;
    this.tasks = this.tasks.map((task) => {
      return (
        (task.id === id && {
          ...task,
          status: status,
        }) ||
        task
      );
    });
    task.status = status;
    return task;
  }
}
